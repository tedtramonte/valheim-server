FROM steamcmd/steamcmd:ubuntu-18@sha256:8557d89ea2515cc978efde28a17eb8c80ee2239bec6a8f2a92c9a35a9e2a1d26

ENV SERVER_NAME="Valheim Server"
ENV SERVER_WORLD="Dedicated"
ENV SERVER_PASSWORD=
ENV SERVER_VISIBILITY=1
ENV SERVER_SAVE_INTERVAL=1800
ENV SERVER_BACKUPS=4
ENV SERVER_BACKUP_SHORT=7200
ENV SERVER_BACKUP_LONG=43200

WORKDIR /root

RUN /root/.steam/steamcmd/steamcmd.sh +force_install_dir /root/valheim-server +login anonymous +app_update 896660 +quit

COPY --chmod=775 entrypoint.sh ./

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 2456-2458/tcp
EXPOSE 2456-2458/udp
