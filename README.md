# Valheim Server

Valheim Server is an OCI container image for hosting a dedicated server for the game [Valheim](https://store.steampowered.com/app/892970/Valheim/).

> :exclamation: **Deprecation Notice**: Beginning **2022-09-06**, the GitLab Container Repository for this image is deprecated. No further images will be published there and they will be gradually removed. The images are simply too large and use up too much of my GitLab storage. The Docker Hub and Quay repositories are still supported (as long as their storage remains unlimited). I recommend switching to the Quay repository, regardless.

## Features

- Automatically rebuilt using [Steamtrigger](https://gitlab.com/tedtramonte/steamtrigger) when the server is updated
  - This means the server can be launched as a specific version, in case players want to play a specific version
  - Can still be automatically updated using [Watchtower](https://github.com/containrrr/watchtower/) or similar tools

## Requirements

- An OCI compliant container engine like Docker or Podman
- Forward ports 2456-2458/UDP to the server
- Open ports 2456-2458/UDP on your firewall
- `$SERVER_PASSWORD` cannot be empty
- `$SERVER_PASSWORD` must be 5 characters or longer
- `$SERVER_PASSWORD` cannot be in `$SERVER_NAME`

## Usage

```bash
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 2456-2458:2456-2458/udp -v /path/to/server-data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:latest

# Windows
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 2456-2458:2456-2458/udp -v C:\Path\To\Server Data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:latest

# Run a specific build
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 2456-2458:2456-2458/udp -v /path/to/server-data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:6176121
```

Or, if you prefer `docker-compose`:

```yaml
services:
  valheim_server:
    image: quay.io/tedtramonte/valheim-server:latest
    restart: unless-stopped
    ports:
      - "2456-2458:2456-2458/udp"
    environment:
      SERVER_NAME: "My Server"
      SERVER_WORLD: "worldsavename"
      SERVER_PASSWORD: "password"
    volumes:
      # Bind mount, to access the files directly on the host
      - /path/to/server-data:/root/.config/unity3d/IronGate/Valheim
      # Ephemeral volume, to persist data without caring about access from the host
      # - valheim_server_data:/root/.config/unity3d/IronGate/Valheim
# Uncomment if using ephemeral volume
# volumes:
#   valheim_server_data:
```

## Configuration

### Environment Variables

- `SERVER_NAME` - The name of the server as it should appear in the server browser (Default: "Valheim Server")
- `SERVER_WORLD` - The name of the `.fwl` and `.db` files used to store the world (Default: "Dedicated")
- `SERVER_PASSWORD` - The password to enter the server
  - This **CANNOT BE BLANK, SHORTER THAN 5 CHARACTERS, OR CONTAINED IN THE SERVER NAME**
- `SERVER_VISIBILITY` - Whether or not to show the server in the server browser (Default: 1)
  - `1` - Visible in browser
  - `0` - Invisible, only joinable by "Join IP"
- `SERVER_SAVE_INTERVAL` - How often the world will save in seconds (Default: 1800)
- `SERVER_BACKUPS` - How many automatic backups will be kept (Default: 4)
- `SERVER_BACKUP_SHORT` - The interval between the first automatic backups (Default: 7200)
- `SERVER_BACKUP_LONG` - The interval between the subsequent automatic backups (Default: 43200)

### Ports

If ports 2456-2458/UDP are in use on your server, you can use a different port range by changing the left side of the port assignment like so:

```bash
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 3556-3558:2456-2458/udp -v /path/to/server-data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:latest
```

### Volumes

In order to persist your server's data between restarts, the `/root/.config/unity3d/IronGate/Valheim` directory must be kept in a volume. A bind mount is recommended to easily adjust the files yourself, but if you don't need to transfer files into the container an ephemeral volume will work as well.

```bash
# Bind mount: prefs, access lists, and worlds dir will be accessible on this path
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 3556-3558:2456-2458/udp -v /path/to/server-data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:latest

# Ephemeral volume: data will persist but be inaccessible from outside the container
docker run -d -e "SERVER_NAME=My Server" -e "SERVER_WORLD=worldsavename" -e "SERVER_PASSWORD=password" -p 3556-3558:2456-2458/udp -v my-valheim-server-data:/root/.config/unity3d/IronGate/Valheim quay.io/tedtramonte/valheim-server:latest
```

## Contributing

Merge requests are welcome after opening an issue first.
